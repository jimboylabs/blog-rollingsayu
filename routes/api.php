<?php

use Illuminate\Http\Request;
use App\Http\Controllers\Api;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider and all of them will
| be assigned to the "api" middleware group. Make something great!
|
*/

Route::middleware('auth:sanctum')->get('/user', function (Request $request) {
    return $request->user();
});

Route::post('queue:work', function () {
    $output = Artisan::call('queue:work', ['--stop-when-empty']);

    return response()->json(['msg' => $output]);
});

Route::apiResource('fragments', Api\FragmentsController::class)
    ->only(['store'])
    ->middleware('auth:api');
