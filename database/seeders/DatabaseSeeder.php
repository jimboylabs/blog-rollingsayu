<?php

namespace Database\Seeders;

// use Illuminate\Database\Console\Seeds\WithoutModelEvents;
use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Str;

class DatabaseSeeder extends Seeder
{
    protected function tables()
    {
        return collect(DB::select("SELECT name FROM sqlite_master WHERE type='table' ORDER BY name;"))
            ->map(fn ($table) => $table->name)
            ->reject(fn ($table) => in_array($table, ['sqlite_sequence', 'migrations', 'sqlite_sequence', 'users']))
            ->reject(fn ($table) => Str::contains($table, 'litestream'));
    }

    protected function truncateTables()
    {
        Schema::disableForeignKeyConstraints();
        $this
            ->tables()
            ->each(function ($table) {
                DB::table($table)->truncate();
                $this->command->info("Truncating: $table");
            });
        Schema::enableForeignKeyConstraints();
    }

    /**
     * Seed the application's database.
     */
    public function run(): void
    {
        // \App\Models\User::factory(10)->create();

        // \App\Models\User::factory()->create([
        //     'name' => 'Test User',
        //     'email' => 'test@example.com',
        // ]);

        $this->truncateTables();

        collect([
            'PHP',
            'Livewire',
            'Golang',
            'htmx',
            'Homelab',
            'Linux',
        ])
        ->each(function ($category) {
            DB::table('categories')->insert([
                'title' => $category,
                'slug' => Str::slug($category),
            ]);
        });
    }
}
