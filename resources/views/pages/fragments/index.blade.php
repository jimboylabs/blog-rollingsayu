<?php

use App\Models\Fragment;
use function Livewire\Volt\{state};

state([
    'fragments' => Fragment::all(),
]);

?>

<x-guest-layout>
    <section class="container mx-auto mt-8">
        @volt('fragments')
            <ul>
                @foreach ($fragments as $fragment)
                    <li wire:key="{{ $fragment->id }}">
                        <a href="/fragments/{{ $fragment->slug }}" wire:navigate class="hover:underline">{{ $fragment->title }}</a>
                    </li>
                @endforeach
            </ul>
        @endvolt
    </section>
</x-guest-layout>
