<?php

use App\Models\Project;
use function Livewire\Volt\{state};

state([
    'projects' => Project::all(),
]);

?>

<x-guest-layout>
    <section class="container mx-auto mt-8">
        @volt('projects')
            <ul>
                @foreach ($projects as $project)
                    <li wire:key="{{ $project->id }}">
                        <a href="{{ $project->url }}" target="_blank" class="hover:underline">{{ $project->name}}</a>
                    </li>
                @endforeach
            </ul>
        @endvolt
    </section>
</x-guest-layout>
