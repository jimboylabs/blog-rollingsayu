#!/bin/sh

set -eux

echo "Migrating the database..."

/app/blog php-cli artisan migrate --force

echo "Database migrated."

echo "All set frankenphp started."

/app/blog php-server
