FROM oven/bun:1 AS asset-builder

COPY package.json bun.lockb /app/

WORKDIR /app

RUN bun install

COPY . .

RUN bun run build

FROM dunglas/frankenphp:static-builder-1.0.3 AS builder

# Copy your app
WORKDIR /go/src/app/dist/app

COPY . .

COPY --from=asset-builder /app/public/build /go/src/app/dist/app/public/build

RUN composer install --ignore-platform-reqs --no-dev

# Build the static binary, be sure to select only the PHP extensions you want
WORKDIR /go/src/app/

RUN EMBED=dist/app/ \
    ./build-static.sh

FROM alpine:latest AS final

WORKDIR /app

COPY --from=builder /go/src/app/dist/frankenphp-linux-x86_64 blog
COPY --from=builder /go/src/app/dist/app/Dockerfile.extras/entrypoint.sh .

RUN chmod +x ./entrypoint.sh

ENTRYPOINT ["./entrypoint.sh"]
