<?php

namespace App\Filament\Resources;

use App\Filament\Resources\FragmentResource\Pages;
use App\Filament\Resources\FragmentResource\RelationManagers;
use App\Models\Fragment;
use Filament\Forms;
use Filament\Forms\Components\TextInput;
use Filament\Forms\Form;
use Filament\Resources\Resource;
use Filament\Tables;
use Filament\Tables\Columns\TextColumn;
use Filament\Tables\Table;
use FilamentTiptapEditor\TiptapEditor;
use Illuminate\Support\Str;
use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\SoftDeletingScope;

class FragmentResource extends Resource
{
    protected static ?string $model = Fragment::class;

    protected static ?string $navigationIcon = 'heroicon-o-rectangle-stack';

    public static function form(Form $form): Form
    {
        return $form
            ->schema([
                TextInput::make('title')
                        ->live(debounce: 500)
                        ->required()
                        ->maxLength(150)
                        ->afterStateUpdated(function(string $operation, $state, $set) {
                            if ($operation == 'edit') {
                                return;
                            }

                            $set('slug', Str::slug($state));
                        }),

                    TextInput::make('slug')->required()->unique(ignoreRecord: true),

                    TiptapEditor::make('body')
                        ->profile('default')
                        ->disk('s3')
                        ->directory('post/images')
                        ->maxFileSize(2048)
                        ->maxContentWidth('full')
                        ->required()
            ]);
    }

    public static function table(Table $table): Table
    {
        return $table
            ->columns([
                TextColumn::make('title')->sortable()->searchable(),
                TextColumn::make('slug')->sortable(),
            ])
            ->filters([
                //
            ])
            ->actions([
                Tables\Actions\EditAction::make(),
            ])
            ->bulkActions([
                Tables\Actions\BulkActionGroup::make([
                    Tables\Actions\DeleteBulkAction::make(),
                ]),
            ]);
    }

    public static function getRelations(): array
    {
        return [
            //
        ];
    }

    public static function getPages(): array
    {
        return [
            'index' => Pages\ListFragments::route('/'),
            'create' => Pages\CreateFragment::route('/create'),
            'edit' => Pages\EditFragment::route('/{record}/edit'),
        ];
    }
}
